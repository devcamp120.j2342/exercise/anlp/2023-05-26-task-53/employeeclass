import models.Employee;

public class App {
    public static void main(String[] args) throws Exception {
        
        Employee employee1 = new Employee(1, "an", "le", 20000000);
        Employee employee2 = new Employee(2, "nguyen", "le", 100000000);

        System.out.println(employee1.toString());
        System.out.println(employee1.getName());
        System.out.println(employee1.getAnnualSalary());
        System.out.println(employee1.raiseSalary(30f));

        System.out.println(employee2.toString());
        System.out.println(employee2.getName());
        System.out.println(employee2.getAnnualSalary());
        System.out.println(employee2.raiseSalary(50f));

    }
}
